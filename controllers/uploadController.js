'use strict'
const formidable = require ('formidable')
const path = require('path')
const fs = require('fs')

exports.uploadFile = (req,res) => {
    var form = new formidable.IncomingForm();
    form.uploadDir = path.join(__dirname, '../public/uploads'); 
    form.keepExtensions = true;
    form.multiples = true;
    form.parse(req, function(err, fields, files) {                  
    });
    
    form.on('file', function(field, file) {    
    });

    form.on('error', function(err) {
        console.log('An error has occured: \n' + err);
    });
    form.on('end', function() {  
        res.send("OK")
    }); 
}
