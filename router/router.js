'use strict'
const express = require ('express')
const router = express.Router()
const app = express()
const uploadController = require('../controllers/uploadController')

app.route('/upload')
    .post(uploadController.uploadFile)

module.exports = app