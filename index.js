const port = 3001
const path = require('path')
const express = require ('express')
const router = require ('./router/router')
const app = express()

app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', "*");
  res.header('Access-Control-Allow-Methods','GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  next();
})

app.use (router)
app.use(express.static(path.join(__dirname, 'public')));
app.get('/', function(req, res){
  res.sendFile(path.join(__dirname, 'views/index.html'));
});

app.listen (port, () => {console.log(`Server up ${port}`)})