
$('#singleFileButton').on('click', function (){
        $('#upload-single').click(); 
});
$('#multipleFileButton').on('click', function (){
        $('#upload-multiple').click(); 
});

$('#upload-single').on('change', function(){
    var file = $(this)[0].files[0];
    console.log(file)
    var formData = new FormData();
    if (file.size > 0){
        formData.append('file', file);
        launch_uploadFile(formData);
    }
});

$('#upload-multiple').on('change', function(){
    var files = $(this).get(0).files;
    console.log(files)
    var formData = new FormData();
    if (files.length > 0){
        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            formData.append('multipleFiles', file, file.name);
        }
        launch_uploadFile(formData);
    }
});

function launch_uploadFile(formData) {
    $.ajax({
        url: '/upload',
        type: 'POST',
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            console.log(data);
            $('.progress-bar').width('0%');
            $('#progressLabel').text('0%');
        },
        xhr: function() {
        // create an XMLHttpRequest
        var xhr = new XMLHttpRequest();
        // listen to the 'progress' event
        xhr.upload.addEventListener('progress', function(evt) {
            if (evt.lengthComputable) {
                // calculate the percentage of upload completed
                var percentComplete = evt.loaded / evt.total;
                percentComplete = parseInt(percentComplete * 100);
                // update the Bootstrap progress bar with the new percentage           
                $('.progress-bar').width(percentComplete + '%');
                $('#progressLabel').text(percentComplete + '%');  
            }
        }, false);
        return xhr;
        }
    });
}